# fcc-tribute-page

- FreeCodeCamp Responsive Web Design Certification. Project #1: Tribute page.
- [Project to be solved.](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-tribute-page)
- [Solution to project.](https://jserranodev.gitlab.io/fcc-tribute-page/)
