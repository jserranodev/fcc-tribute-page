module.exports = function (eleventyConfig) {
  eleventyConfig.setTemplateFormats(["html", "css", "js", "jpg", "png"]);
};
